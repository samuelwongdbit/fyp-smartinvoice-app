import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  public files: any[];
  public imagePath;
  imgURL: any;
  public message: string;
  constructor() { this.files = []; }

// onFileChanged(event: any) {
//   this.files = event.target.files;
// }

// onUpload() {
//   const formData = new FormData();
//   for (const file of this.files) {
//       formData.append(name, file, file.name);
//   }
//   // this.http.post('url', formData).subscribe(x => ....);
// }

preview(files) {
  if (files.length === 0)
    return;

  var mimeType = files[0].type;
  if (mimeType.match(/image\/*/) == null) {
    this.message = "Only images are supported.";
    return;
  }

  var reader = new FileReader();
  this.imagePath = files;
  reader.readAsDataURL(files[0]); 
  reader.onload = (_event) => { 
    this.imgURL = reader.result; 
  }
}

  ngOnInit() {
  }

}
